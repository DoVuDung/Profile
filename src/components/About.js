import React from "react";

export default function About() {
  return (
    <main>
      <p>
        This application contains snippets from various Codecademy articles. You can browse all our articles and read them in their entirety <a href="https://drive.google.com/drive/u/0/folders/116OtauYurgVCCPMUmtTvmNhLDudH9ONU">here</a>.
      </p>
    </main>
  );
}
